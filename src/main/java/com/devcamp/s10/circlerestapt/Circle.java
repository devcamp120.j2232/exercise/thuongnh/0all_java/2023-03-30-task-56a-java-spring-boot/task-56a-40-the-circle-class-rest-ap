package com.devcamp.s10.circlerestapt;

public class Circle {
    private double radius = 1.0;   // bán kính hình tròn 

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {      
        this.radius = radius;
    }
    // tính diện tích 
    public double getArena() {
        return Math.pow(this.radius, 2) * Math.PI;
    }
    // tính chu vi 
    public double getCircumference() {
        return 2 * Math.PI * this.radius;
    }

    public String toString(){
        return  "circle [radius = " +  this.radius + "]";
    }
    
   



    
}


