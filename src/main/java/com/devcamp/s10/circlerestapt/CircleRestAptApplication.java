package com.devcamp.s10.circlerestapt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircleRestAptApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircleRestAptApplication.class, args);
	}

}
