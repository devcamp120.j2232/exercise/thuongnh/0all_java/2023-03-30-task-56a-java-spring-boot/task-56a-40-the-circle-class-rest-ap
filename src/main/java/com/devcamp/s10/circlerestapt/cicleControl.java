package com.devcamp.s10.circlerestapt;


import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class cicleControl {

    @GetMapping("/circle-area")
    public ArrayList<Double> getEmployees(){

        //  khởi tạo 3 đối tượng sinh viên 

        Circle hinhTron1 = new Circle(1);
        Circle hinhTron2 = new Circle(2);
        Circle hinhTron3 = new Circle(3);
        // in thông tin của ba nhân viên
        System.out.println(hinhTron1);
        System.out.println(hinhTron2);
        System.out.println(hinhTron3);
        // thêm ba nhân viên vào 1 danh sách
        System.out.println(hinhTron1);
        System.out.println(hinhTron2);
        System.out.println(hinhTron3);

        ArrayList<Double> circleArr = new ArrayList<Double>();
        circleArr.add(hinhTron1.getArena());
        circleArr.add(hinhTron2.getArena());
        circleArr.add(hinhTron3.getArena());
        return circleArr;
    }
    
}